 import java.util.*;
import java.io.*;
class Client implements Comparable<Client>{

    private int id;
    private int arrival;
    private int service=0;

private boolean eInCoada=false;
    public Client(int id,int arrival,int service)
    {
        this.id=id;
        this.arrival=arrival;
        this.service=service;
    }

    public int getId()
    {
        return id;
    }

    public int getArrival()
    {
        return arrival;
    }

    public int getService()
    {
        return service;
    }

    public boolean getEInCoada()
    {
        return eInCoada;
    }
    public void setEInCoada(boolean e)
    {
        eInCoada=e;
    }

    public void setId(int id)
    {
        this.id=id;
    }


    public void print(PrintWriter pw)
    {
        pw.printf(" (%d %d %d) ",id,arrival,service);
    }


    public void setService(int i)
    {
        this.service=i;
    }

    public int compareTo(Client a)
    {
        if(a.getArrival()>this.getArrival())
            return -1;

        if(a.getArrival()<this.getArrival())
            return 1;

        return 0;
    }
}
