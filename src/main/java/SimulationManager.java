import java.util.*;
import java.io.*;
public class SimulationManager implements Runnable {

    private int nrClienti;
    private int nrCozi;
    private int timpSimulare;

    private int minArrival;
    private int maxArrival;

    private int minService;
    private int maxService;

    private LinkedList<Client> clienti = new LinkedList<Client>();

    private LinkedList<Server> servers = new LinkedList<>();


    private Scheduler scheduler;

private File intrare;
private File iesire;
PrintWriter pw;

    public SimulationManager(String stringIn,String stringOut) {

        intrare=new File(stringIn);
        try {
            iesire = new File(stringOut);
            if (!iesire.exists()) {
                iesire.createNewFile();
            }

            pw=new PrintWriter(iesire);
        }
    catch(IOException e){}


        int[] numere=new int[100];
       numere= readFromString(intrare);

        this.nrClienti=numere[0];
        this.nrCozi=numere[1];
        this.timpSimulare=numere[2];
       this.minArrival=numere[3];
        this.maxArrival=numere[4];
       this.minService=numere[5];
        this.maxService=numere[6];

        scheduler = new Scheduler(nrCozi);
        clienti = generateNClients(nrClienti);
    }

    public int[] readFromString(File file)
    {
        int[] numere=new int[100];
        int i=0;

        try {
            Scanner s1 = new Scanner(file);

            while(s1.hasNext()) {
                List<String> segmente=Arrays.asList(s1.next().split(","));

                for(String segment:segmente) {
                    try {
                        numere[i] = Integer.parseInt(segment);
                        i++;
                    }
                    catch(NumberFormatException e){}
                }
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Nu s-a putut deschide fisierul de intrare");
            System.exit(10);
        }
        if(i!=7)
        {
            System.out.println("format gresit");
            System.exit(100);
        }
        return numere;
    }

    public LinkedList<Client> generateNClients(int n) {
        int i;
        for (i = 1; i <= n; i++) {
            int arrival = new Random().nextInt((this.maxArrival - this.minArrival) + 1) + this.minArrival;
            int service = new Random().nextInt((this.maxService - this.minService + 1)) + this.minService;
            Client a = new Client(i, arrival, service);

            clienti.add(a);
        }
        Collections.sort(clienti, ClientSortTime.bestTime);

        return this.clienti;
    }

    public void printWaitingClients()
    {
        for(Client a:clienti)
        {
            a.print(pw);
        }
        pw.printf(";");
        pw.println();
    }

    public int totalTime()
    {
        int total=0;
        for(Server e:servers)
        {
            total=total+e.getRunningTIME();

        }
        return total;
    }


    public void printCozi(LinkedList<Server> servers)
    {
        for(Server e:servers)
        {
            if(e.getWait()>0)
            {pw.printf("Coada %d:",e.getIdCoada());
            e.printClienti(pw);}

            else{
                pw.printf("Coada %d: Closed",e.getIdCoada());
            }
            pw.println();
        }
    }

    public synchronized void run() {
        try {
            for (int currenttime = 0; currenttime < timpSimulare; currenttime++) {
                pw.println("Time: " + currenttime); pw.printf("Waiting clients: ");
                Iterator<Client> it = clienti.iterator();

                while (it.hasNext()) {
                    Client a = it.next();

                    if (a.getArrival() <= currenttime ) {
                        scheduler.dispatchClient(a);

                        if(a.getEInCoada()==true)
                            it.remove();
                    }
                }
                servers=scheduler.getServere();
                printWaitingClients();
                printCozi(servers);
                Thread.sleep(1000);

               pw.println();
               pw.flush();
            }
        }
        catch(InterruptedException e){}
        pw.println("Average time: "+(float)totalTime()/(float)nrClienti);
        pw.close();
        System.exit(1);
        }
    }






