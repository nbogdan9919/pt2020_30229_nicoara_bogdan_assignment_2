import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.io.*;

public class Server implements Runnable,Comparable<Server>{

private BlockingQueue<Client> clienti=new LinkedBlockingQueue<Client>();
private AtomicInteger waittime;

private int idCoada;

private int ocupat=0;

private int runningTIME;



public Server(int idCoada)
{
    this.idCoada=idCoada;
}


public Server()
{

   waittime=new AtomicInteger(0);

}

public int getWait()
{
    return waittime.get();
}
public int getOcupat()
{

    return ocupat;
}
public int getIdCoada()
{
    return idCoada;
}

public int getRunningTIME()
{
    return runningTIME;
}
public void setRunningTIME(int a)
{
    runningTIME=a;
}
public void setOcupat(int i)
{
    ocupat=i;
}

    public void setIdCoada(int id)
    {
        idCoada=id;
    }

public void addClient(Client a)
{
  clienti.add(a);
    waittime.set(waittime.intValue()+a.getService());

}
public void removeClient(Client a)
{
    clienti.remove(a);
    waittime.decrementAndGet();

}
    public int clientiSize()
    {
        return clienti.size();
    }

public void printClienti(PrintWriter pw)
{
    for(Client a:clienti)
    {
        a.print(pw);
    }
}

public synchronized  void run() {

    try{

        Client a = clienti.take();
        clienti.add(a);//echivalent cu getFirst dintr-o lista normala

    while (waittime.get()>0)
    {
        this.ocupat=1;

            Thread.sleep(1000);

            a.setService(a.getService() - 1);
            runningTIME++;
            waittime.decrementAndGet();

            if (a.getService() == 0) {
                this.ocupat=0;
                clienti.remove(a);

                a=clienti.peek();
                //a=clienti.take();
                //clienti.addFirst(a);
            }

        }
    }
    catch (InterruptedException e) {

    }

   }



    public int compareTo(Server a)
{
    if(a.getIdCoada()>this.getIdCoada())
    {
        return -1;
    }
    if(a.getIdCoada()<this.getIdCoada())
    {
        return 1;
    }

    return 0;
}

}
