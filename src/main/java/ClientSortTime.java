import java.util.Comparator;
public class ClientSortTime{

        static final Comparator<Client> bestTime=new Comparator<Client>() {

            public int compare(Client e1, Client e2) {
                if (e1.getArrival()> e2.getArrival())
                    return 1;

                if (e1.getArrival() < e2.getArrival())
                    return -1;

                return 0;

            }
        };
    }

